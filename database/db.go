package database

// Database contains all the methods required to allow handling different
// key-value data stores.
type Database interface {
	KVReader
	KVWriter
}

// KVReader represents a key-value reader exposing a Has and Get method
type KVReader interface {
	// Has checks and retrieves [key] in data store
	Has(key []byte) (bool, error)
	// Get retrieves an object with [key] from the data store
	Get(key []byte) ([]byte, error)
}

// KVWriter represents a key-value writer for a data store
type KVWriter interface {
	// Put inserts an object [value] with [key]
	Put(key []byte, value []byte) error
	// Update updates the value of [key]
	Update(key []byte, value []byte) error
	// Delete removes [key] from the key-value data store
	Delete(key []byte) error
}
