# LID-server

Lid is a digital currency that allows fast payment to anywhere in the world. It is a cryptocurrency that focuses on facilitating trade, and giving equal 
opportunity to everyone. We believe money is a right and it is as impoortant as food foor survival. Lid uses a distributed ledger system to manage transactions
and issue money.

# License

# Development 
Everyone is welcome to contribute, the *master* branch has the latest code. There will be version branches, for exaple V1, V2, v3. 
Developers who frequently contribute to the software will recieve tokens from the netwoork as theyy have iinvested their time and energy into building the system.

# Testing
There are two types of tests that we run, they are manual testing and automated testing. Develoopers can checkout latest builds and spawn various servers,
and make requests too them. Developers can also wriite tests and push them as prooposals too the Lid development team.

*How to setup local network testing*

You can simulate a real life sittuatiion, where you have many servers on the network.
- The first thing you need to do is to create a folder hosting all yoour test servers. Call this "test servers"
- Create folfers in test servers called "server1, server2, server3 ...." (I advise 6)
- On your lid server project, hit the server_list.json file and add " http://localhost:100/api/v1/,  http://localhost:200/api/v1/ ...." as much as the test servers you want 
- Back in the "server1 folder create .env file and set PORT variable. For server 1 have port 100, server 2 port 200 server 3.... each server runs on its own port.
- In each server folder add a server_list.json folder and add the same urls
- Run go build to build a new lid-server binary
- In your lid server project run update.sh script. this will move the recently built lid server binary to each of the folders. 
- start a new terminal froom each of the server folders
- on each of the terminals run "./lid-server" to run the servers
- Start the server "go run" on your project
- Use post man to make requests

*Note: In the future, all these processes foor setting up a test scene will be automated with a bash script*
