package node

// Config represents all the configurations of a LID Node
type Config struct {
	// Server peers configurations
	ServerListPath string

	// Environments configs
	EnvConfigPath string

	// Wallets directory
	WalletsDir string

	// Build configurations
	BuildExecutable string `mapstructure:"BUILD_EXEC"`
	BuildHash       string

	// HTTP Configuration
	HTTPHost string
	HTTPPort string `mapstructure:"PORT"`

	// Networking
	IP string

	// Rate Limitting
	MaxBucketSize            int `mapstructure:"RATE_LIMITTER_BUCKET_SIZE"`
	RateLimitCleanupInterval int `mapstructure:"RATE_LIMITTER_CLEANUP_TIME"`

	// Validator Node configurations
	ValidatorWalletAddress string `mapstructure:"VALIDATOR_WALLET"`

	// LID Bank configurations
	LIDBankWalletAddress string
	LIDRewardSender      string

	// Bootstrapping
	BootstrappingPeer string `mapstructure:"BOOTSTRAPPING_PEER"`
	Peers             []*Peer
	NodeType          string `mapstructure:"NODE_TYPE"`

	PriceDB string
}
