package logging

import "testing"

func TestLog(t *testing.T) {
	log, err := New(Config{
		DefaultLogDir,
		"TEST",
	})
	if err != nil {
		t.Fatalf("Error creating log: %s", err)
	}
	log.Info("Test Log")
}
